plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
    id("org.jetbrains.dokka")
    id("maven-publish")
}

val versionMajor = 1
val versionMinor = 1
val versionPatch = 2

val versionName = "${versionMajor}.${versionMinor}.${versionPatch}"
val baseName = "vard4android-$versionName"

android {
    namespace = "at.bitfire.vcard4android"

    compileSdk = 33

    defaultConfig {
        minSdk = 21        // Android 5

        aarMetadata {
            minCompileSdk = 29
        }
        setProperty("archivesBaseName", baseName)
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17

        isCoreLibraryDesugaringEnabled = true
    }
    kotlin {
        jvmToolchain(17)
    }

    buildFeatures {
        buildConfig = true
    }

    buildTypes {
        release {
            isMinifyEnabled = false
        }
    }

    packaging {
        resources {
            excludes += listOf("LICENSE", "META-INF/LICENSE.txt", "META-INF/NOTICE.txt")
        }
    }

    lint {
        disable += listOf("AllowBackup", "InvalidPackage")
    }

    defaultConfig {
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    publishing {
        // Configure publish variant
        singleVariant("release") {
            withSourcesJar()
        }
    }
}

publishing {
    // Configure publishing data
    publications {
        create<MavenPublication>("maven") {
            groupId = "foundation.e.lib" // or io.murena?
            artifactId = "vcard4android"
            version = versionName
            artifact("$buildDir/outputs/aar/$baseName-release.aar")

            pom.withXml {
                val dependenciesNode = asNode().appendNode("dependencies")
                configurations["implementation"].dependencies.forEach { dependency ->
                    if (dependency.name != "unspecified") {
                        val dependencyNode = dependenciesNode.appendNode("dependency")
                        dependencyNode.appendNode("groupId", dependency.group)
                        dependencyNode.appendNode("artifactId", dependency.name)
                        dependencyNode.appendNode("version", dependency.version)
                    }
                }
            }

            repositories {
                maven {
                    url = uri("https://gitlab.e.foundation/api/v4/projects/96/packages/maven")
                    name = "GitLab"
                    credentials(HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }
                    authentication {
                        create("header", HttpHeaderAuthentication::class)
                    }
                }
            }
        }
    }
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib")
    coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:2.0.3")

    implementation("androidx.annotation:annotation:1.6.0")
    @Suppress("GradleDependency")
    implementation("commons-io:commons-io:2.6")
    implementation("org.apache.commons:commons-text:1.3")

    // ez-vcard to parse/generate vCards
    api("com.googlecode.ez-vcard:ez-vcard:0.12.1") {    // requires Java 8
        // hCard functionality not needed
        exclude(group = "org.jsoup")
        exclude(group = "org.freemarker")
    }

    androidTestImplementation("androidx.test:runner:1.5.2")
    androidTestImplementation("androidx.test:rules:1.5.0")

    testImplementation("junit:junit:4.13.2")
}